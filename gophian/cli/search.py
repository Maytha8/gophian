# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import click
import requests

from gophian.packages import DebianGolangPackages


@click.command()
@click.argument("query")
@click.pass_context
def search(context: click.Context, query: str):
    """
    Search Debian Go packages by name and import path.
    """

    session: requests.Session = context.obj

    packages = DebianGolangPackages(session)
    for package in filter(
        lambda p: query in p["source"]
        or query in p["binary"]
        or query in p["metadata_value"],
        packages.packages,
    ):
        importpaths = list(
            map(lambda x: x.strip(), package["metadata_value"].split(","))
        )
        click.echo(package["binary"] + " -- " + ", ".join(importpaths))
