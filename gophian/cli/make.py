# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import re
import subprocess
import tarfile
from enum import Enum, auto
from pathlib import Path
from datetime import datetime
from shutil import rmtree
from typing import List, Optional, Tuple
from urllib.parse import urlparse

import click
from gophian.git import Git
import questionary
import requests
from jinja2 import Environment, PackageLoader, select_autoescape
from infer_license.api import guess_file

from gophian.__about__ import BUGS_URL
from gophian.error import GbpExecutionError
from gophian.hosts import KNOWN_HOSTS
from gophian.licenses import KNOWN_LICENSES, license_text
from gophian.name import debianize_name, shorten_host
from gophian.packages import DebianGolangPackages
from gophian.salsa import salsa_repo_exists
from gophian.session import Session
from gophian.vcs import get_short_desc, package_from_import_path
from gophian.wnpp import ITPBug, RFPBug


@click.command()
@click.argument("importpath")
@click.option("--quiet/--no-quiet", default=False, help="Don't print warnings.")
@click.option(
    "--type",
    "package_type_opt",
    required=False,
    type=click.Choice(["l", "l+b", "b+l", "b"], case_sensitive=False),
    help="Type of software being packaged.",
)
@click.option(
    "--name",
    required=True,
    envvar="DEBFULLNAME",
    help="Name of package maintainer. Uses DEBFULLNAME environment variable by default.",
)
@click.option(
    "--email",
    required=True,
    envvar="DEBEMAIL",
    help="Email of package maintainer. Uses DEBEMAIL environment variable by default.",
)
@click.option(
    "--ignore-packaged/--no-ignore-packaged",
    default=False,
    help="Ignore whether the given import path is already packaged in Debian.",
)
@click.option(
    "--ignore-salsa/--no-ignore-salsa",
    default=False,
    help="Ignore whether the package already has a Salsa repo.",
)
@click.option("--itp/--no-itp", default=True, help="Generate an ITP template as well.")
@click.pass_context
def make(
    context: click.Context,
    importpath: str,
    quiet: bool,
    package_type_opt: Optional[str],
    name: str,
    email: str,
    ignore_packaged: bool,
    ignore_salsa: bool,
    itp: bool,
):
    """
    Prepare a Go module for packaging in Debian.
    """

    requests_session: requests.Session = context.obj

    if not quiet:
        click.secho("gophian is experimental software!", bold=True, fg="yellow")
        click.secho("Please report any problems to:", fg="yellow")
        click.secho(BUGS_URL, fg="yellow")

    with MakeSession(requests_session) as session:
        try:
            go_package, git_repo = package_from_import_path(
                requests_session, importpath
            )
        except Exception as error:
            click.echo(error)
            click.secho("Did you specify a Go package import path?", fg="yellow")
            return

        if go_package != importpath:
            click.echo(
                f"Continuing with repo root {go_package} instead of {importpath}"
            )

        debian_packages = DebianGolangPackages(requests_session)

        if debian_packages._check_for_package(go_package):
            if not ignore_packaged:
                if not quiet:
                    click.secho(
                        "Pass --ignore-packaged to ignore this warning and continue.",
                        fg="cyan",
                    )
                return

        host = go_package.lower().split("/")[0]
        if host in KNOWN_HOSTS:
            short_host = KNOWN_HOSTS[host]
        else:
            short_host = shorten_host(host)
            if not quiet:
                click.secho(
                    f"Using '{short_host}' as canoncial hostname for '{host}'",
                    fg="yellow",
                )
                click.secho(
                    "If this is not suitable, please change it manually and report the issue to https://salsa.debian.org/Maytha8/gophian/issues",
                    fg="yellow",
                )

        debian_source_package = debianize_name(go_package, short_host)

        debian_library_package = debian_source_package + "-dev"

        try:
            all_binaries: List[str] = session.package_main_modules(go_package)
        except Exception:
            all_binaries: List[str] = session.find_main_modules(go_package)

        if not quiet and len(all_binaries) > 0:
            click.echo("Found binaries:")
            for binary in all_binaries:
                click.echo(f" - {binary.split('/')[-1]} ({binary})")

        wnpp = None

        binary_word = "binaries" if len(all_binaries) > 1 else "binary"

        if package_type_opt is not None:
            package_type: PackageType = PackageType.from_opt(package_type_opt)
        elif len(all_binaries) == 0:
            package_type: PackageType = PackageType.LIBRARY
        else:
            package_type: PackageType = questionary.select(
                "What type of software are you packaging?",
                choices=[
                    questionary.Choice(
                        "Library only", value=PackageType.LIBRARY, checked=True
                    ),
                    questionary.Choice(
                        f"Library and accompanying {binary_word}",
                        value=PackageType.LIBRARY_AND_BINARY,
                    ),
                    questionary.Choice(
                        f"{binary_word.capitalize()} and accompanying library",
                        value=PackageType.BINARY_AND_LIBRARY,
                    ),
                    questionary.Choice(
                        f"{binary_word.capitalize()} only", value=PackageType.BINARY
                    ),
                ],
            ).ask()
            if package_type is None:
                raise click.Abort()

        selected_binaries: List[str] = []

        if package_type.has_binary():
            if len(all_binaries) > 1:
                binary_choices: List[questionary.Choice] = list(
                    map(
                        lambda b: questionary.Choice(
                            f"{b.split('/')[-1]} ({b})", value=b
                        ),
                        all_binaries,
                    )
                )
                selected_binaries = questionary.checkbox(
                    "Which binaries would you like to include?", choices=binary_choices
                ).ask()
                if selected_binaries is None:
                    raise click.Abort()
            else:
                selected_binaries = all_binaries

        if (
            package_type == PackageType.BINARY_AND_LIBRARY
            or package_type == PackageType.BINARY
        ):
            choices: List[str] = list(
                map(lambda b: b.split("/")[-1], selected_binaries)
            )

            if git_repo.split("/")[-1] not in choices:
                choices.insert(0, git_repo.split("/")[-1])

            choices.insert(0, debian_source_package)

            debian_source_package = questionary.select(
                "What do you want to name the source package?", choices=choices
            ).ask()
            if debian_source_package is None:
                raise click.Abort()
        if not quiet and salsa_repo_exists(
            requests_session, "go-team/packages/" + debian_source_package
        ):
            click.secho(
                f"Salsa repo of the same name exists for '{debian_source_package}'.",
                fg="yellow",
            )
            click.echo(
                f"https://salsa.debian.org/go-team/packages/{debian_source_package}"
            )
            if not ignore_salsa:
                click.secho("To ignore, pass the --ignore-salsa flag.", fg="cyan")
                return

        if bug := ITPBug.get(requests_session, debian_source_package):
            wnpp = bug
            if not quiet:
                click.secho(f"Found wnpp bug for {debian_source_package}", fg="cyan")
                click.secho(
                    f"[#{bug.id}] ITP: {bug.package} -- {bug.description}", fg="cyan"
                )
        elif bug := RFPBug.get(requests_session, debian_source_package):
            wnpp = bug
            if not quiet:
                click.secho(f"Found wnpp bug for {debian_source_package}", fg="cyan")
                click.secho(
                    f"[#{bug.id}] RFP: {bug.package} -- {bug.description}", fg="cyan"
                )

        work_dir = Path(debian_source_package)

        if work_dir.exists():
            click.secho(
                f"Output directory '{work_dir}' already exists",
                fg="red",
            )
            context.exit(1)

        work_dir.mkdir()

        version, versioned, repack, tarball = session.prepare_upstream(
            go_package,
            debian_source_package,
        )

        repo = Git.init(work_dir, "debian/sid")
        repo.remote(
            f"git@salsa.debian.org:go-team/packages/{debian_source_package}.git"
        )

        out = subprocess.run(
            [
                "gbp",
                "import-orig",
                "--no-interactive",
                "--debian-branch=debian/sid",
                "--filter=/debian/**/*",
                os.path.join("..", tarball),
            ],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
            cwd=repo.path,
        )
        if out.returncode != 0:
            raise GbpExecutionError(out)

        with open(work_dir / ".gitignore", "a") as file:
            file.write("\n/.pc/\n/_build/")

        repo.add([".gitignore"])
        repo.commit("Add quilt and build directories to gitignore")

        debian_deps = []
        debian_build_deps = ["debhelper-compat (= 13)", "dh-golang", "golang-any"]

        try:
            go_deps = session.package_deps(go_package)
            for dep, _ in go_deps:
                if debian_package := debian_packages.library_is_packaged(dep):
                    debian_deps.append(debian_package)
                else:
                    if not quiet:
                        click.secho(
                            f"Dependency {dep} is not packaged in Debian!", fg="yellow"
                        )

            debian_build_deps += debian_deps

            go_test_deps = session.package_deps(go_package, True)
            for dep, _ in go_test_deps:
                if dep in go_deps:
                    continue
                elif debian_package := debian_packages.library_is_packaged(dep):
                    if debian_package not in debian_deps:
                        debian_build_deps.append(debian_package + " <!nocheck>")
                    else:
                        continue
                else:
                    if not quiet:
                        click.secho(
                            f"Test dependency {dep} is not packaged in Debian!",
                            fg="yellow",
                        )
        except Exception as e:
            click.echo(e)
            click.secho(
                "An error occurred fetching dependencies, skipping...", fg="red"
            )

        short_desc = get_short_desc(requests_session, git_repo)

        if (work_dir / "go.mod").exists():
            PACKAGE_NAME_REGEX = re.compile(r"^package (\S+)$")
            match = PACKAGE_NAME_REGEX.search((work_dir / "go.mod").read_text())
            if match:
                go_import_path = match.group(1)
            else:
                go_import_path = go_package
        else:
            go_import_path = go_package

        (work_dir / "debian").mkdir()

        env = Environment(
            loader=PackageLoader("gophian"),
            autoescape=select_autoescape(),
            trim_blocks=True,
            lstrip_blocks=True,
            keep_trailing_newline=True,
        )
        header_template = env.get_template("control/header.jinja")

        control_stanzas = []

        control_stanzas.append(
            header_template.render(
                source=debian_source_package,
                uploaders=f"{name} <{email}>",
                lib_deps=debian_build_deps,
                homepage=git_repo,
                go_import_path=go_import_path,
            )
        )

        match package_type:
            case PackageType.LIBRARY:
                control_stanzas.append(
                    render_library_package(
                        env, debian_library_package, short_desc, debian_deps
                    )
                )
            case PackageType.BINARY:
                for binary in selected_binaries:
                    control_stanzas.append(
                        render_binary_package(env, binary.split("/")[-1], short_desc)
                    )
            case PackageType.LIBRARY_AND_BINARY:
                control_stanzas.append(
                    render_library_package(
                        env, debian_library_package, short_desc, debian_deps
                    )
                )
                for binary in selected_binaries:
                    control_stanzas.append(
                        render_binary_package(env, binary.split("/")[-1], short_desc)
                    )
            case PackageType.BINARY_AND_LIBRARY:
                for binary in selected_binaries:
                    control_stanzas.append(
                        render_binary_package(env, binary.split("/")[-1], short_desc)
                    )
                control_stanzas.append(
                    render_library_package(
                        env, debian_library_package, short_desc, debian_deps
                    )
                )

        control = "\n\n".join(control_stanzas)

        with (work_dir / "debian/control").open("x") as file:
            file.write(control)

        lic = "TODO"

        for file in work_dir.glob("*"):
            if file.name.lower() in [
                "license",
                "license.md",
                "copying",
                "copying.md",
                "unlicense",
            ] and (guess := guess_file(str(file))):
                if guess.shortname in KNOWN_LICENSES:
                    lic = KNOWN_LICENSES[guess.shortname]
                else:
                    lic = "TODO"
                break

        copyr_template = env.get_template("copyright.jinja")

        with (work_dir / "debian/copyright").open("x") as file:
            file.write(
                copyr_template.render(
                    license=lic,
                    license_text=license_text(lic),
                    year=datetime.now().strftime("%Y"),
                    name=name,
                    email=email,
                    homepage=git_repo,
                    upstream_name=go_package.split("/")[-1],
                    repack=repack,
                )
            )

        changelog_template = env.get_template("changelog.jinja")

        with (work_dir / "debian/changelog").open("x") as file:
            file.write(
                changelog_template.render(
                    source=debian_source_package,
                    debian_version=version + "-1",
                    itp=wnpp.id if wnpp is not None else "TODO",
                    name=name,
                    email=email,
                    date=datetime.now().strftime("%a, %d %b %Y %H:%M:%S")
                    + " "
                    + datetime.utcnow().astimezone().strftime("%z")[:5],
                )
            )

        with (work_dir / "debian/.gitignore").open("x") as file:
            file.write(
                "*.debhelper\n"
                "*.log\n"
                "*.substvars\n"
                "/.debhelper/\n"
                "/debhelper-build-stamp/\n"
                "/files\n"
            )
            if package_type.has_binary():
                for binary in selected_binaries:
                    file.write("/" + binary.split("/")[-1] + "/\n")
            if package_type.has_library():
                file.write("/" + debian_library_package + "/\n")

        with (work_dir / "debian/rules").open("x") as file:
            file.write(
                "#!/usr/bin/make -f\n"
                "\n"
                "%:\n"
                "\tdh $@ --builddirectory=_build --buildsystem=golang --with=golang\n"
            )
            if package_type == PackageType.BINARY:
                file.write(
                    "\n"
                    "override_dh_auto_install:\n"
                    "\tdh_auto_install -- --no-source\n"
                )

        (work_dir / "debian/rules").chmod(0o0755)

        (work_dir / "debian/source").mkdir()

        with (work_dir / "debian/source/format").open("x") as file:
            file.write("3.0 (quilt)\n")

        with (work_dir / "debian/gbp.conf").open("x") as file:
            file.write("[DEFAULT]\n" "debian-branch = debian/sid\n" "dist = DEP14\n")

        if (
            package_type == PackageType.BINARY_AND_LIBRARY
            or package_type == PackageType.LIBRARY_AND_BINARY
        ):
            with (work_dir / f"debian/{debian_library_package}.install").open(
                "x"
            ) as file:
                file.write("usr/share\n")
            if len(selected_binaries) > 1:
                for binary in selected_binaries:
                    with (work_dir / f"debian/{binary.split('/')[-1]}.install").open(
                        "x"
                    ) as file:
                        file.write(f"usr/bin/{binary.split('/')[-1]}\n")
            else:
                with (
                    work_dir / f"debian/{selected_binaries[0].split('/')[-1]}.install"
                ).open("x") as file:
                    file.write("usr/bin\n")

        with (work_dir / "debian/watch").open("x") as file:
            file.write(gen_debian_watch(env, git_repo, versioned, repack))

        (work_dir / "debian/upstream").mkdir()

        with (work_dir / "debian/upstream/metadata").open("x") as file:
            file.write(gen_debian_upstream_metadata(git_repo))

        with (work_dir / "debian/gitlab-ci.yml").open("x") as file:
            file.write(
                """# auto-generated, DO NOT MODIFY.
# The authoritative copy of this file lives at:
# https://salsa.debian.org/go-team/infra/pkg-go-tools/blob/master/config/gitlabciyml.go
---
include:
  - https://salsa.debian.org/go-team/infra/pkg-go-tools/-/raw/master/pipeline/test-archive.yml
"""
            )

        if itp:
            itp_template = env.get_template("itp.jinja")
            with Path("itp.txt").open("x") as file:
                file.write(
                    itp_template.render(
                        name=name,
                        email=email,
                        package=debian_source_package,
                        short_desc=short_desc,
                        long_desc=(
                            "TODO: long description, what you'd write in debian/control\n"
                            "      keep it indented, just like in the control file"
                        ),
                        version=version,
                        upstream="TODO",
                        url=git_repo,
                        license=lic,
                    )
                )

        if quiet:
            return

        click.secho(
            "Packaging prepared successfully in " + str(work_dir),
            bold=True,
            fg="green",
        )
        click.echo("")
        click.echo("Resolve all the TODOs in debian/, find them using:")
        click.echo("    grep -r TODO debian/")
        click.echo("")
        click.echo("To build the package, its recommended to use sbuild:")
        click.echo("    sbuild")
        click.echo("See https://wiki.debian.org/sbuild#Setup for setup instructions.")
        click.echo("")
        click.echo("When you finish packaging, commit your changes:")
        click.echo("    git add debian && git commit -S -m 'Initial packaging'")
        click.echo("")
        click.echo("To create the packaging repo on Salsa, use:")
        click.echo("    dh-make-golang create-salsa-project " + debian_source_package)
        click.echo("")
        click.echo("Once you are happy with your work, push to Salsa:")
        click.echo("    git push --all && git push --tags")
        click.echo("    # or")
        click.echo("    gbp push")
        click.echo("")

        # Print experimental warning again, in case the logs drowned it out the first time.
        click.secho("gophian is experimental software!", bold=True, fg="yellow")
        click.secho("Please report any problems to:", fg="yellow")
        click.secho(BUGS_URL, fg="yellow")


def gen_debian_watch(env: Environment, repo: str, versioned: bool, repack: bool) -> str:
    url = urlparse(repo)
    if versioned:
        if url.hostname == "github.com":
            template = env.get_template("watch/github.jinja")
            return template.render(repo=repo, repack=repack)
        template = env.get_template("watch/git_tags.jinja")
        return template.render(repo=repo, repack=repack)
    else:
        template = env.get_template("watch/git_commits.jinja")
        return template.render(repo=repo, repack=repack)


def gen_debian_upstream_metadata(repo: str) -> str:
    url = urlparse(repo)
    if url.hostname == "github.com" or url.hostname == "codeberg.org":
        if url_exists(repo + "/issues"):
            return (
                "---\n"
                "Bug-Database: " + repo + "/issues\n"
                "Bug-Submit: " + repo + "/issues/new\n"
                "Repository: " + repo + ".git\n"
                "Repository-Browse: " + repo + "\n"
            )
        else:
            return (
                "---\n"
                "Repository: " + repo + ".git\n"
                "Repository-Browse: " + repo + "\n"
            )
    if url.hostname == "gitlab.com" or url.hostname == "salsa.debian.org":
        if url_exists(repo + "/-/issues"):
            return (
                "---\n"
                "Bug-Database: " + repo + "/-/issues\n"
                "Bug-Submit: " + repo + "/-/issues/new\n"
                "Repository: " + repo + ".git\n"
                "Repository-Browse: " + repo + "\n"
            )
        else:
            return (
                "---\n"
                "Repository: " + repo + ".git\n"
                "Repository-Browse: " + repo + "\n"
            )
    return (
        "---\n"
        "Bug-Database: TODO\n"
        "Bug-Submit: TODO\n"
        "Repository: " + repo + ".git\n"
        "Repository-Browse: " + repo + "\n"
    )


def url_exists(url: str) -> bool:
    res = requests.head(url, timeout=60)
    return res.status_code == 200


def render_library_package(
    env: Environment, package: str, short_desc: str, lib_deps: List[str]
) -> str:
    library_template = env.get_template("control/go_library.jinja")
    return library_template.render(
        package=package,
        lib_deps=lib_deps,
        short_desc=short_desc,
    )


def render_binary_package(env: Environment, package: str, short_desc: str) -> str:
    binary_template = env.get_template("control/go_binary.jinja")
    return binary_template.render(
        package=package,
        short_desc=short_desc,
    )


class PackageType(Enum):
    LIBRARY = auto()
    BINARY = auto()
    LIBRARY_AND_BINARY = auto()
    BINARY_AND_LIBRARY = auto()

    @classmethod
    def from_opt(cls, opt: str):
        match opt:
            case "l":
                return cls.LIBRARY
            case "b":
                return cls.BINARY
            case "l+b":
                return cls.LIBRARY_AND_BINARY
            case "b+l":
                return cls.BINARY_AND_LIBRARY
            case _:
                return cls.LIBRARY

    def has_library(self) -> bool:
        return (
            self == self.__class__.LIBRARY
            or self == self.__class__.LIBRARY_AND_BINARY
            or self == self.__class__.BINARY_AND_LIBRARY
        )

    def has_binary(self) -> bool:
        return (
            self == self.__class__.BINARY
            or self == self.__class__.LIBRARY_AND_BINARY
            or self == self.__class__.BINARY_AND_LIBRARY
        )


class MakeSession(Session):
    def find_main_modules(self, go_package: str) -> List[str]:
        self.go_get(go_package)

        repo_path = self.gopath / "src" / go_package

        PACKAGE_REGEX = re.compile(r"^package main$")

        main_modules: List[str] = []

        for path in repo_path.rglob("*.go"):
            with path.open() as file:
                for line in file:
                    if line.strip() == "":
                        continue
                    elif match := PACKAGE_REGEX.match(line):
                        print(path, match)
                        module = path.parent.relative_to(repo_path)
                        if str(module) not in main_modules:
                            main_modules.append(str(module))
                    else:
                        break
        return main_modules

    def prepare_upstream(
        self, go_package: str, source_package: str
    ) -> Tuple[str, bool, bool, Path]:
        repo = self.go_get(go_package)

        rmtree(repo.path / ".git")

        repack = False

        if (repo.path / "vendor").exists():
            repack = True
            rmtree(repo.path / "vendor")

        version = repo.debianized_version
        if repack:
            version += "+ds"

        tarball_path = Path(f"{source_package}_{version}.orig.tar.xz")

        with tarfile.open(name=tarball_path, mode="x:xz") as tarball:
            tarball.add(repo.path, arcname=source_package)

        return (version, repo.versioned, repack, tarball_path)
