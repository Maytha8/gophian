# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List

import click
import requests
from treelib import Tree

from gophian.__about__ import BUGS_URL
from gophian.error import ExecutionError
from gophian.packages import DebianGolangPackages
from gophian.session import Session
from gophian.vcs import package_from_import_path


@click.command()
@click.argument("importpath")
@click.option("--quiet/--no-quiet", default=False, help="Don't print warnings.")
@click.option(
    "--ignore-packaged/--no-ignore-packaged",
    default=False,
    help="Ignore whether the given Go package has already been packaged for Debian. Also enables --ignore-salsa.",
)
@click.option(
    "--packaged/--no-packaged",
    default=True,
    help="Include packaged dependencies in the estimate.",
)
@click.pass_context
def estimate(
    context: click.Context,
    importpath: str,
    quiet: bool,
    ignore_packaged: bool,
    packaged: bool,
) -> None:
    """
    Estimate work required to Debianize a given Go package.
    """

    requests_session: requests.Session = context.obj

    if not quiet:
        click.secho("gophian is experimental software!", bold=True, fg="yellow")
        click.secho("Please report any problems to:", fg="yellow")
        click.secho(BUGS_URL, fg="yellow")

    try:
        package, _ = package_from_import_path(requests_session, importpath)
    except Exception as error:
        click.echo(error)
        click.secho("Did you specify a Go package import path?", fg="yellow")
        context.exit(1)

    if package != importpath:
        click.echo(f"Continuing with repo root {package} instead of {importpath}")

    debian_packages = DebianGolangPackages(requests_session)

    if debian_packages._check_for_package(package, quiet) and not ignore_packaged:
        if not quiet:
            click.secho("To ignore, pass the --ignore-packaged flag.", fg="cyan")
        return

    with Session(requests_session) as session:
        seen: List[str] = []
        errors: List[ExecutionError] = []

        tree = Tree()
        tree.create_node(package, package)
        dependency_tree(
            session,
            seen,
            errors,
            package,
            packaged,
            debian_packages,
            tree,
        )
        click.echo(tree.show(stdout=False))

        for error in errors:
            click.echo(error)


def dependency_tree(
    session: Session,
    seen: List[str],
    errors: List[ExecutionError],
    package: str,
    show_packaged_deps: bool,
    debian_packages: DebianGolangPackages,
    tree: Tree,
    level: int = 1,
):
    try:
        dep_packages = session.package_deps(package)
    except ExecutionError as error:
        errors.append(error)
        tree.create_node(
            click.style("Error getting dependencies", fg="red"),
            f"{package}_error",
            parent=package,
        )
        return
    for dep_package, _ in dep_packages:
        if debian_package := debian_packages.library_is_packaged(dep_package):
            if show_packaged_deps:
                tree.create_node(
                    click.style(
                        f"{dep_package} ({debian_package})",
                        fg="green",
                    ),
                    f"{package}_{dep_package}",
                    parent=package,
                )
        elif dep_package in seen:
            tree.create_node(
                click.style(f"{dep_package} (seen)", fg="blue"),
                f"{package}_{dep_package}",
                parent=package,
            )
        else:
            tree.create_node(
                dep_package,
                dep_package,
                parent=package,
            )
            seen.append(dep_package)
            dependency_tree(
                session,
                seen,
                errors,
                dep_package,
                show_packaged_deps,
                debian_packages,
                tree,
                level + 1,
            )
