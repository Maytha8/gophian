# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import click
import requests

from gophian.packages import DebianGolangPackages


@click.command()
@click.argument("query")
@click.pass_context
def show(context: click.Context, query: str):
    """
    Show Debian Go package by name or import path.
    """

    session: requests.Session = context.obj

    packages = DebianGolangPackages(session)

    package = next(
        filter(
            lambda p: query == p["source"]
            or query == p["binary"]
            or query == p["metadata_value"],
            packages.packages,
        )
    )

    click.echo(click.style("Source: ", bold=True) + package["source"])
    click.echo(click.style("Import path: ", bold=True) + package["metadata_value"])
    click.secho("Packages:", bold=True)
    for binary_package in filter(
        lambda p: p["source"] == package["source"]
        and not p["binary"].endswith("-dbgsym"),
        packages.packages,
    ):
        click.echo(" - " + binary_package["binary"])
