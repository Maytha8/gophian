# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path

# Dict that maps SPDX license names to Debian license names
# This should list all text files in the licenses/ directory
KNOWN_LICENSES = {
    "MIT": "Expat",
    "AGPL-3.0-or-later": "AGPL-3+",
    "GPL-2.0-or-later": "GPL-2+",
    "GPL-3.0-or-later": "GPL-3+",
    "LGPL-2.0-or-later": "LGPL-2+",
    "LGPL-2.1-or-later": "LGPL-2.1+",
    "LGPL-3-or-later": "LGPL-3+",
    "Apache-2.0": "Apache-2.0",
    "BSD-2-Clause": "BSD-2-Clause",
    "BSD-3-Clause": "BSD-3-Clause",
}


def license_text(lic: str) -> str:
    license_file = Path(__file__).parent / f"{lic}.txt"
    if license_file.exists():
        return license_file.read_text()
    else:
        return " TODO: Unknown license"
