# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from typing import Optional

from gophian.hosts import KNOWN_HOSTS


def debianize_name(package: str, short_host: Optional[str] = None) -> str:
    parts = package.lower().split("/")
    if short_host is None:
        short_host = KNOWN_HOSTS[parts[0]]
    parts[0] = short_host
    return "golang-" + "-".join(parts)


def shorten_host(host: str) -> str:
    return ".".join(host.split(".")[:-1])
