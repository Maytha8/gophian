# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
from datetime import datetime
from typing import TYPE_CHECKING, Dict

from packaging.version import Version

from gophian.git import Git

if TYPE_CHECKING:
    from gophian.session import Session


class GoPackage(Git):
    def __init__(self, session: "Session", package: str, repo: str) -> None:
        self.path = session.gopath / "src" / package
        self.session = session

        super().__init__(self.path, quiet=True, check=False)
        self._clone(repo)

        tags: Dict[Version, str] = {}
        all_tags = self.tags()
        for tag in all_tags:
            if match := re.search(r"^(v?(\d[\d\.]+))$", tag, re.MULTILINE):
                tags[Version(match.group(1))] = match.group(0)

        versions = list(tags.keys())
        versions.sort()

        if len(versions) > 0:
            version = versions[-1]
            commitish = tags[version]
            self.versioned = True
            self.debianized_version = str(version)
            self.checkout(commitish)
        else:
            iso_date, commit_hash = self.get(["log", "-1", "--format=%cI %H"]).split(
                " "
            )
            date = datetime.fromisoformat(iso_date).strftime("%Y%m%d")
            self.versioned = False
            self.debianized_version = f"0.0~git{date}.{commit_hash[:7]}"
