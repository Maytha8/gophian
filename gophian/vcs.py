# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import re
from typing import Tuple
import requests
from gophian.error import GophianError
from urllib.parse import quote as urlencode, urlparse

GO_IMPORT_META_REGEX = re.compile(
    r"<meta\s+name=\"?go-import\"?\s*content=\"(\S*) git (\S*)\"\s*/?>"
)


def package_from_import_path(session: requests.Session, path: str) -> Tuple[str, str]:
    """
    Get the root package and repo from the given import path.
    """
    if path.split("/")[0] == "github.com":
        repo = "/".join(path.split("/")[0:3])
        return (repo, strip_git_suffix("https://" + repo))
    try:
        return get_repo_meta(session, path)
    except NoRootRepoError:
        raise NoRootRepoError(path)


def get_repo_meta(session: requests.Session, path: str) -> Tuple[str, str]:
    """
    Get the root package and repo by recursively downloading and checking
    website pages for a 'go-import' meta tag.
    """
    res = session.get("https://" + path, timeout=60, params={"go-get": "1"})
    match = GO_IMPORT_META_REGEX.search(res.text)
    if match is None:
        parts = path.split("/")
        if len(parts[:-1]) < 1:
            raise NoRootRepoError(path)
        else:
            return get_repo_meta(session, "/".join(parts[:-1]))
    return (
        match.group(1),
        strip_git_suffix(match.group(2)),
    )


def get_short_desc(session: requests.Session, repo_url: str) -> str:
    url = urlparse(repo_url)
    path = url.path[1:]
    desc = None

    if url.hostname == "github.com":
        repo = session.get(f"https://api.github.com/repos/{path}", timeout=60).json()
        if repo["description"] is not None and len(repo["description"]) > 0:
            desc = repo["description"]

    if url.hostname == ["codeberg.org", "gitea.com"]:
        repo = session.get(
            f"https://{url.hostname}/api/v1/repos/{path}", timeout=60
        ).json()
        if repo["description"] is not None and len(repo["description"]) > 0:
            desc = repo["description"]

    if url.hostname in ["salsa.debian.org", "gitlab.com"]:
        repo = session.get(
            f"https://{url.hostname}/api/v4/projects/{urlencode(path)}", timeout=60
        ).json()
        if repo["description"] is not None and len(repo["description"]) > 0:
            desc = repo["description"]

    if desc is None:
        return "TODO"

    desc = remove_punctuation(desc)
    desc = remove_article(desc)
    desc = lower_first_letter(desc)
    return desc


def remove_punctuation(s: str) -> str:
    if s[-1] in [".", "!", "?"]:
        return s[:-1]
    else:
        return s


def remove_article(s: str) -> str:
    if s.lower().startswith("a "):
        return s[2:]
    elif s.lower().startswith("an "):
        return s[3:]
    else:
        return s


def lower_first_letter(s: str) -> str:
    return s[0].lower() + s[1:]


def strip_git_suffix(s: str) -> str:
    if s.endswith(".git/"):
        return s[:-5]
    if s.endswith(".git"):
        return s[:-4]
    return s


class NoRootRepoError(GophianError):
    def __init__(self, path: str):
        super().__init__(f"No root repo could be determined for the path '{path}'")
