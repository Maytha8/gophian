# gophian

**Tools to help with Debianizing Go software**

Experimental software, please report any issues to
https://codeberg.org/Maytha8/gophian/issues. Alternatively, you can email me at
`maytha8thedev@gmail.com`.

In my experience, dh-make-golang is a little underfeatured and isn't very
reliable. `dh-make-golang make` doesn't work half the time, it doesn't
automatically fill in (Build-)Depends, and `dh-make-golang estimate` doesn't
work at all ([Debian#1050523](https://bugs.debian.org/1050523),
[Debian#1058936](https://bugs.debian.org/1058936)).

This is because dh-make-golang still relies on the deprecated and removed
golang.org/x/tools/go/vcs package to work out the root repo of import paths
([Debian#1043070](https://bugs.debian.org/1043070),
[dh-make-golang#200](https://github.com/Debian/dh-make-golang/issues/200)).

gophian seeks to change that, by avoiding the use of `go get` completely (and
replicating it with the session module), and providing something that *just
works*, as well as adding on more features and more intelligence. This CLI aims
to be a full alternative to dh-make-golang.

**Hey Go team,** what do you think of this tool?

## Features

- `gophian make`
  - Check if it's already been packaged for Debian, and whether it's in the
    Package Tracker or the NEW queue.
  - Check for an ITP/RFP, tell you about it, and fill in debian/changelog
  - Check for a Salsa repo with the name of the source package
  - Uses Jinja templates for d/control, d/changelog, d/watch, and d/copyright
    instead of creating them inside the program.
  - Checks for common licenses in LICENSE and COPYING, and fills them in for
    you in d/copyright.
  - Prompts you for package type.
  - Prompts you for source package name (if you're package is binary-only or
    binary-and-library)
  - Asks you which binaries you'd like to include, ensuring that you can
    exclude testing binaries.
  - Extra short hostnames for websites like codeberg.org.
- `gophian estimate`
  - Reliable alternative to `dh-make-golang`
  - Can exclude packaged dependencies, giving you a TODO list of unpackaged Go
    libraries.
  - `gophian estimate-graphviz` (graphviz feature only)
    - Visualise the dependency tree using graphviz.

## Install

```sh
pipx install git+https://codeberg.org/Maytha8/gophian.git
# or with graphviz support
pipx install git+https://codeberg.org/Maytha8/gophian.git[graphviz]
```

## Runtime Dependencies

This program requires `git` and `go` in order to run.

## Optional Features

- `graphviz` - enables the estimate-graphviz command

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md).

## Packaging for Debian

You think this is worth packaging, and being included in Debian? Please let me
know (see contact info below).

## Contact

Report any bugs or issues to https://codeberg.org/Maytha8/gophian/issues

Alternatively, you can email me at `maytha8thedev@gmail.com`.

## License

```
gophian -- tools to help with Debianizing Go software
Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
