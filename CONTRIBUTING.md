# Contributing

All contributions are welcome, and they don't have to be code contributions!

- [Issues](#issues)
- [Hacking](#hacking)
- [Documentation](#documentation)

## Issues

Finding problems or missing features and reporting them to
https://codeberg.org/Maytha8/gophian is greatly appreciated!

If you don't have a Codeberg account and are not interested in creating one,
then you can email me instead at `maytha8thedev@gmail.com`

## Hacking

**Please see the NOTES below these instructions!**

First of all, make sure to run `pre-commit install`

Then:
```sh
# with a venv
sudo apt install python3-venv
python3 -m venv .venv
source .venv/bin/activate # or .venv/bin/activate.(fish|csh)
pip install -e .[dev,graphviz]
# or with pipx
pipx install -e .[dev,graphviz]
```

All files in the gophian/ directory **MUST** have the copyright and license
header (templates and license text files are exempt). So make sure to add it to
any new files. You can copy it from the end of this document.

### NOTES

The Session clones repos into `self.gopath`, but this **is not representative
of nor usable as an actual GOPATH**. This is because packages are cloned
according to **their repository root** and not the 'package' statement in
go.mod.

This means that packages such as `github.com/go-git/go-git/v5` will not be
cloned in the correct location, but rather
`GOPATH/src/github.com/go-git/go-git`.

Do **NOT** rely on Session.gopath for running `go get`, `go build`, or `go
install`. (You shouldn't need to run these commands anyway.)

If there's a new feature that requires a correctly-structured GOPATH, please
let me know so I can refactor the code accordingly.

## Documentation

Coming soon...

I plan on creating a kind-of wiki with all the info I can harvest from various
sources within the Debian Go team, and a detailed tutorial/guide on how to use
gophian.

Btw I hate the rst format, I'll be using Markdown no matter what. Probably will
use python3-markdown instead of trying to piece together Sphinx.

## Header

```txt
# gophian -- tools to help with Debianizing Go software
# Copyright (C) 2024 Maytham Alsudany <maytha8thedev@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
